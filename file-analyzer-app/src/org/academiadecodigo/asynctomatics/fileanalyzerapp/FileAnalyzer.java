package org.academiadecodigo.asynctomatics.fileanalyzerapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalyzer {

    public static void main(String[] args) {

        // init my class
        FileAnalyzer fileAnalyzer = new FileAnalyzer();

        // save my two files into a list of strings
        List<String> stringsFile1 = fileAnalyzer.initFile1();
        List<String> stringsFile2 = fileAnalyzer.initFile2();

        if (stringsFile1 != null) {

            // reading file 1
            System.out.println("\n  ** Analyzing song Audition lyrics **");

            // step 1 - counting words
            System.out.println("\nNumber of words: " + fileAnalyzer.readingCountingWords(stringsFile1));

            // step 2 - finding first long word with the provided number of chars
            System.out.println("\nThe first longest word: " + fileAnalyzer.findingFirstLongestWord(stringsFile1, 8));

            // step 3 - finding the provided number of longest words
            System.out.println("\nLongest words are: " + fileAnalyzer.findingLongestWords(stringsFile1, 5));
        }

        if (stringsFile1 != null && stringsFile2 != null) {

            // comparing two files
            System.out.println("\n  ** Analyzing the songs Audition and Memories lyrics **");

            // step 4 - finding common words in the two files
            System.out.println("\nThe common words are: " + fileAnalyzer.commonWordsInFiles(stringsFile1, stringsFile2));
        }
    }

    private List<String> initFile1() {

        try {

            return Files.lines(Paths.get("resources/audition.txt"))
                    .flatMap(line -> Stream.of(line.replaceAll("[^A-Za-z0-9' ]", "")
                            .split(" ")))
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<String> initFile2() {

        try {

            return Files.lines(Paths.get("resources/memories.txt"))
                    .flatMap(line -> Stream.of(line.replaceAll("[^A-Za-z0-9' ]", "")
                            .split(" ")))
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private long readingCountingWords(List<String> stringsFile1) {

        return stringsFile1.stream()
                .count();
    }

    private String findingFirstLongestWord(List<String> stringsFile1, int numChars) {

        return stringsFile1.stream()
                .filter(word -> word.length() >= numChars)
                .findFirst()
                .orElse("No words with " + numChars + " or more.");
    }

    private Set<String> findingLongestWords(List<String> stringsFile1, int limit) {

        return stringsFile1.stream()
                .sorted(Comparator.comparingInt(String::length).reversed())
                .limit(limit)
                .collect(Collectors.toSet());
    }

    private Set<String> commonWordsInFiles(List<String> stringsFile1, List<String> stringsFile2) {

        return stringsFile1.stream()
                .filter(stringsFile2::contains)
                .collect(Collectors.toSet());
    }
}
